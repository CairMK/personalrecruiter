@extends('layouts.app')
@section('content')

    <div class="containers">
        <div class="row">
            <div class="col-12">

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif
                <form action="{{ route('products.store') }}" method="postt" multiple="">
                    <div class="widget">
                        <div class="widget-header bordered-bottom bordered-warning">
                            <span class="widget-caption">Add blog</span>
                        </div>
                        <div class="widget-body">
                            <div id="horizontal-form">

                                {{ Form::model('blog', ['route' => ['blog.store'], 'method' => 'POST', 'files'=>true]) }}
                                {!! csrf_field() !!}
                                <div class="input-group{{ $errors->has('image') ? ' has-error' : '' }}">
							<span class="input-group-btn">
								<span class="btn btn-info shiny btn-file">
									<i class="btn-label fa fa-image"> </i> Browse... <input type="file" name="image">
								</span>
							</span>
                                    <input type="text" class="form-control" readonly="">
                                </div>
                                <br/>
                                @if ($errors->has('image')) <p
                                        class="alert alert-danger">{{ $errors->first('image') }}</p> @endif

                                <label for="title">@lang('partials.title')</label>
                                <div class="form-group">
                                    <input type="text" class="form-control"
                                           placeholder="title"
                                           name="title">
                                </div>
                                @if ($errors->has('title')) <p
                                        class="alert alert-danger">{{ $errors->first('title') }}</p> @endif
                                <label for="description">Blog description</label>

                                <div class="form-group">
                                    <textarea class="ckeditor" id="elm3" name="description"></textarea>
                                </div>
                                @if ($errors->has('description')) <p
                                        class="alert alert-danger">{{ $errors->first('description') }}</p> @endif

                                <button type="submit" class="btn btn-labeled shiny btn-warning btn-large"><i
                                            class="btn-label fa fa-plus"></i> Create
                                </button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('admin.partials.editor')
@endsection

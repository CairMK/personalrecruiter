<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * Public routes
 */
Route::get('/', 'HomeController@index');
Route::get('blogs/', 'HomeController@blog')->name('blog');
Route::get('blogs/{slug}', 'HomeController@blog_post')->name('blog_post');
/**
 * Protected routes
 */
Route::group(['middleware' => ['web'], 'namespace' => 'Admin'], function () {
    Auth::routes();
    Route::resource('blog', 'BlogController');
});
<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blogs')->delete();

        $faker = Faker::create('App\Models\Blog');

        for ($i = 1; $i <= 100; $i++) {
            DB::table('blogs')->insert([
                'title' => $faker->sentence(),
                'description' => $faker->text($maxNbChars = 3000),
                'slug' => $faker->slug(),
                'image' => $faker->imageUrl(),
                'created_at' => \Carbon\Carbon::now(),
                'Updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
}

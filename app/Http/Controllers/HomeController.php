<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function blog()
    {

        $blog = Blog::paginate(3);
        return view('blog', compact('blog'));
    }
}

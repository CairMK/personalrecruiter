<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Blog\Store;
use App\Http\Requests\Blog\Update;
use App\Models\Blog;
use App\Services\BlogService;
use App\Traits\ImageUpload;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Input;
use Session;


class BlogController extends Controller
{
    use ImageUpload;

    /**
     * @var BlogService
     */
    protected BlogService $blogService;

    function __construct(BlogService $blogService)
    {
        $this->middleware('auth');

        $this->blogService = $blogService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $blog = $this->blogService->index();
        return view('admin.blog.index', compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $data = $this->blogService->create();
        return view('admin.blog.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     *
     * @return RedirectResponse
     */
    public function store(Store $request)
    {
        $blog = $this->blogService->store($request);

        Session::flash('flash_message', 'Main blog successfully created!');

        return redirect()->route('blog.edit', $blog);

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Blog $blog
     *
     * @return Factory|View
     */
    public function edit(Blog $blog)
    {
        $this->blogService->edit($blog);
        return view('admin.blog.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param Blog $blog
     *
     * @return RedirectResponse
     */
    public function update(Update $request, Blog $blog)
    {
        $blog = $this->blogService->update($request, $blog);
        Session::flash('flash_message', 'Main blog successfully edited!');

        return redirect()->route('blog.edit', $blog);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Blog $blog
     *
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function destroy(Blog $blog)
    {
        $this->blogService->destroy($blog);
        return redirect(route('blog.index'));
    }

}

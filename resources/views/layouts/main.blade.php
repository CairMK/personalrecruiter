<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- CSRF Token -->
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="format-detection" content="telephone=no"/>
    <link rel='stylesheet' href='/assets/vendor/revslider/settings.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/js/custom/trx/trx_addons_icons-embedded.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/vendor/swiper/swiper.min.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/js/custom/trx/trx_addons.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/vendor/js_comp/js_comp_custom.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/fonts/Carnas/stylesheet.css' type='text/css' media='all'/>
    <link rel='stylesheet'
          href='http://fonts.googleapis.com/css?family=Open+Sans%3A400%2C800italic%2C300%2C300italic%2C400italic%2C600%2C600italic%2C700%2C700italic%2C800&amp;subset=latin%2Clatin-ext&amp;ver=4.7.3'
          type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/css/fontello/css/fontello.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/css/style.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/css/animation.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/css/colors.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/css/styles.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/css/responsive.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/css/custom.css' type='text/css' media='all'/>
    <link rel='stylesheet' href='/assets/vendor/js_comp/font-awesome.min.css' type='text/css' media='all'/>
    @yield('style')
    <link rel="icon" href="/assets/images/cropped-fav-big-32x32.jpg" sizes="32x32"/>
    <link rel="icon" href="/assets/images/cropped-fav-big-192x192.jpg" sizes="192x192"/>
    <link rel="apple-touch-icon-precomposed" href="/assets/images/cropped-fav-big-180x180.jpg"/>
    <meta name="msapplication-TileImage" content="/assets/images/cropped-fav-big-270x270.jpg"/>
</head>
<body class="homepg home page body_style_wide scheme_default blog_style_excerpt sidebar_hide expand_content remove_margins header_style_header-2 header_title_off no_layout vc_responsive">
@include('layouts.menu')
@yield('content')
@include('layouts.footer')
<script type='text/javascript' src='/assets/vendor/jQuery/jquery.js'></script>
<script type='text/javascript' src='/assets/js/custom/custom.js'></script>
<script type='text/javascript' src='/assets/vendor/jQuery/jquery-migrate.min.js'></script>
<script type='text/javascript' src='/assets/vendor/revslider/jquery.themepunch.tools.min.js'></script>
<script type='text/javascript' src='/assets/vendor/revslider/jquery.themepunch.revolution.min.js'></script>
<script type='text/javascript'
        src='/assets/vendor/revslider/extensions/revolution.extension.slideanims.min.js'></script>
<script type='text/javascript' src='/assets/vendor/revslider/extensions/revolution.extension.actions.min.js'></script>
<script type='text/javascript'
        src='/assets/vendor/revslider/extensions/revolution.extension.layeranimation.min.js'></script>
<script type='text/javascript'
        src='/assets/vendor/revslider/extensions/revolution.extension.navigation.min.js'></script>
<script type='text/javascript' src='/assets/vendor/swiper/swiper.jquery.min.js'></script>
<script type='text/javascript' src='/assets/js/custom/trx/trx_addons.js'></script>
<script type='text/javascript' src='/assets/js/custom/scripts.js'></script>
<script type='text/javascript' src='/assets/js/custom/embed.min.js'></script>
<script type='text/javascript' src='/assets/vendor/js_comp/js_comp.min.js'></script>
@yield('script')
<a href="#" class="trx_addons_scroll_to_top trx_addons_icon-up" title="Scroll to top"></a>
</body>
</html>


@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">

            <div class="col-12">
                <br>
                <p><a class="btn btn-labeled shiny btn-warning btn-large" href="{{ route('blog.create')}}"> <i
                                class="btn-label fa fa-plus"></i>@lang('partials.add')</a></p>
            </div>

            <div class="bg-white table-responsive rounded shadow-sm mb-30">
                <table class="table mb-0 table-striped" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>@lang('partials.status')</th>
                        <th>@lang('partials.image')</th>
                        <th>@lang('partials.title')</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach( $blog  as $blogs )
                        <tr>
                            <td> @if($blogs->image)
                                    <img src="/admin/img/blog/thumbnails/{{ $blogs->image }}">
                                @endif
                            </td>
                            <td>{{ $blogs->title }}</td>
                            <td><span class="time"><a
                                            href="{{ route('blog.edit',$blogs->id) }}"
                                            class="btn btn-info">@lang('partials.edit')</a></span></td>
                            <td>   {{ Form::model('blog', ['route' => ['blog.destroy', $blogs->id], 'method' => 'DELETE', 'id' => 'delete'])}}
                                {!! csrf_field() !!}

                                <button type="submit" class="btn btn-labeled shiny btn-danger delete"><i
                                            class="btn-label fa fa-trash"></i> @lang('partials.delete')
                                </button>
                                {{ Form::close() }}</td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>

            </div>
@endsection

<?php


namespace App\Services;


use App\Http\Requests\Blog\Store;
use App\Http\Requests\Blog\Update;
use App\Models\Blog;
use App\Traits\ImageUpload;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Str;

class BlogService
{
    use ImageUpload;

    public function index()
    {
        return Blog::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Blog
     */
    public function create()
    {
        return new Blog();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Store $request
     *
     * @return Blog|Model
     */
    public function store(Store $request)
    {

        $request['title'] = strip_tags($request['title']);
        $request['slug'] = Str::slug($request['title']);
        $request['subtitle'] = strip_tags($request['subtitle']);
        $input = $request->all();
        $input['image'] = $this->verifyAndStoreImage($request);

        return Blog::create($input);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Blog $blog
     *
     * @return array
     */
    public function edit(Blog $blog)
    {
        return compact('blog');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Update $request
     * @param Blog $blog
     *
     * @return Blog|Model
     */
    public function update(Update $request, Blog $blog)
    {
        $request['title'] = strip_tags($request['title']);
        $request['slug'] = Str::slug($request['title']);
        $request['subtitle'] = strip_tags($request['subtitle']);

        $input = $request->all();

        $input['image'] = $this->verifyAndStoreImage($request);

        return Blog::create($input);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Blog $blog
     *
     * @return void
     * @throws Exception
     */
    public function destroy(Blog $blog)
    {
        if ($blog->image) {
            $paths = $this->makePaths();
            $image = $paths->original . $blog->image;
            $image_medium = $paths->medium . $blog->image;
            $image_thumb = $paths->thumbnail . $blog->image;

            if (file_exists($image)) {
                unlink($image);
            }
            if (file_exists($image_medium)) {
                unlink($image_medium);
            }
            if (file_exists($image_thumb)) {
                unlink($image_thumb);
            }

        }

        $blog->delete();
    }

    public function makePaths()
    {
        $original = public_path() . '/admin/img/blog/';
        $thumbnail = public_path() . '/admin/img/blog/thumbnails/';
        $medium = public_path() . '/admin/img/blog/medium/';
        return (object)compact('original', 'thumbnail', 'medium');
    }
}
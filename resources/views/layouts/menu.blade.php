<div class="body_wrap">
    <div class="page_wrap">
        <header class="top_panel top_panel_style_2 scheme_default">
            <div class="top_panel_fixed_wrap"></div>
            <div class="top_panel_navi scheme_dark">
                <div class="menu_main_wrap clearfix">
                    <div class="content_wrap">
                        <a class="logo scheme_dark" href="/">
                            <img src="images/logo.png" alt="logo" style="max-width: 40%;"></a>
                        <nav class="menu_main_nav_area menu_hover_fade">
                            <div class="menu_main_inner">
                                <div class="contact_wrap scheme_default ">
                                    <div class="phone_wrap icon-mobile">+3 123 123 123</div>
                                    <div class="socials_wrap">
                                            <span class="social_item">
                                                <a href="#" target="_blank" class="social_icons social_twitter">
                                                    <span class="trx_addons_icon-twitter"></span>
                                                </a>
                                            </span>
                                        <span class="social_item">
                                                <a href="#" target="_blank" class="social_icons social_facebook">
                                                    <span class="trx_addons_icon-facebook"></span>
                                                </a>
                                            </span>
                                        <span class="social_item">
                                                <a href="#" target="_blank" class="social_icons social_gplus">
                                                    <span class="trx_addons_icon-gplus"></span>
                                                </a>
                                            </span>
                                    </div>
                                    <div class="search_wrap search_style_fullscreen">
                                        <div class="search_form_wrap">
                                            <form role="search" method="get" class="search_form" action="#">
                                                <input type="text" class="search_field" placeholder="Search" value="" name="s">
                                                <button type="submit" class="search_submit icon-search"></button>
                                                <a class="search_close icon-cancel"></a>
                                            </form>
                                        </div>
                                        <div class="search_results widget_area">
                                            <a href="#" class="search_results_close icon-cancel"></a>
                                            <div class="search_results_content"></div>
                                        </div>
                                    </div>
                                </div>
                                <ul id="menu_main" class="sc_layouts_menu_nav menu_main_nav">
                                    <li class="menu-item"><a href="/"><span>Home</span></a></li>

                                    <li class="menu-item menu-item-has-children"><a href="#"><span>Pages</span></a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="about.html"><span>About Us</span></a></li>
                                            <li class="menu-item"><a href="team.html"><span>Our Advisers</span></a></li>
                                            <li class="menu-item"><a href="single-team.html"><span>Adviser&#8217;s Profile</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item menu-item-has-children"><a href="#"><span>Features</span></a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="support.html"><span>Support</span></a></li>
                                            <li class="menu-item"><a href="video.html"><span>Video Tutorials</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item menu-item-has-children"><a href="#"><span>Blog</span></a>
                                        <ul class="sub-menu">
                                            <li class="menu-item menu-item-has-children"><a><span>Blog Style 1</span></a>
                                            </li>
                                            <li class="menu-item menu-item-has-children"><a><span>Blog Style 2</span></a>
                                            </li>
                                            <li class="menu-item"><a href="post-formats.html"><span>Post Formats</span></a></li>
                                            <li class="menu-item"><a href="single-post.html"><span>Post With Comments</span></a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item"><a href="contacts.html"><span>Contacts</span></a></li>
                                </ul>
                            </div>
                        </nav>
                        <a class="menu_mobile_button"></a>
                    </div>
                </div>
            </div>
        </header>
        <div class="menu_mobile_overlay"></div>
        <div class="menu_mobile scheme_">
            <div class="menu_mobile_inner">
                <a class="menu_mobile_close icon-cancel"></a>
                <nav class="menu_mobile_nav_area">
                    <ul id="menu_mobile" class="sc_layouts_menu_nav menu_main_nav">
                        <li class="menu-item current-menu-ancestor current-menu-parent menu-item-has-children"><a><span>Home</span></a>
                        </li>
                        <li class="menu-item menu-item-has-children"><a href="#"><span>Pages</span></a>
                            <ul class="sub-menu">
                                <li class="menu-item"><a href="about.html"><span>About Us</span></a></li>
                                <li class="menu-item"><a href="team.html"><span>Our Advisers</span></a></li>
                                <li class="menu-item"><a href="services.html"><span>Our Services</span></a></li>
                            </ul>
                        </li>
                        <li class="menu-item menu-item-has-children"><a href="#"><span>Features</span></a>
                        </li>
                        <li class="menu-item menu-item-has-children"><a href="#"><span>Blog</span></a>
                            <ul class="sub-menu">
                                <li class="menu-item menu-item-has-children"><a><span>Blog Style 1</span></a>

                                </li>
                                <li class="menu-item menu-item-has-children"><a><span>Blog Style 2</span></a>

                                </li>
                                <li class="menu-item"><a href="post-formats.html"><span>Post Formats</span></a></li>
                                <li class="menu-item"><a href="single-post.html"><span>Post With Comments</span></a></li>
                            </ul>
                        </li>
                        <li class="menu-item"><a href="contacts.html"><span>Contacts</span></a></li>
                    </ul>
                </nav>
                <div class="search_mobile">
                    <div class="search_form_wrap">
                        <form role="search" method="get" class="search_form" action="#">
                            <input type="text" class="search_field" placeholder="Search ..." value="" name="s">
                            <button type="submit" class="search_submit icon-search" title="Start search"></button>
                        </form>
                    </div>
                </div>
                <div class="socials_mobile">
                        <span class="social_item">
                            <a href="#" target="_blank" class="social_icons social_twitter">
                                <span class="trx_addons_icon-twitter"></span>
                            </a>
                        </span>
                    <span class="social_item">
                            <a href="#" target="_blank" class="social_icons social_facebook">
                                <span class="trx_addons_icon-facebook"></span>
                            </a>
                        </span>
                    <span class="social_item">
                            <a href="#" target="_blank" class="social_icons social_gplus">
                                <span class="trx_addons_icon-gplus"></span>
                            </a>
                        </span>
                </div>
            </div>
        </div>
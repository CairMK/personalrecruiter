<footer class="site_footer_wrap">
    <div class="footer_wrap widget_area scheme_dark">
        <div class="footer_wrap_inner widget_area_inner">
            <div class="content_wrap">
                <div class="columns_wrap">
                    <aside class="column-1_4 widget widget_text">
                        <div class="textwidget">
                            <img src="/assets/images/logo.png" alt="HR Advisor">
                            <ul class="trx_addons_list  icons">
                                <li class="icon-home-alt">123, New Lenox Chicago,
                                    <br/> IL 60606
                                </li>
                                <li class="icon-white"><a href="cdn-cgi/l/email-protection.html"
                                                          class="__cf_email__"
                                                          data-cfemail="335a5d555c735b415257455a4056411d505c5e">[email&#160;protected]</a>
                                </li>
                                <li class="icon-tablet">+3 833 211 32</li>
                            </ul>
                        </div>
                    </aside>
                    <aside class="column-1_4 widget widget_nav_menu">
                        <h5 class="widget_title">Links</h5>
                        <div class="menu-footer-links-container">
                            <ul id="menu-footer-links" class="menu">
                                <li class="menu-item"><a href="about.html">About Us</a></li>
                                <li class="menu-item"><a href="services.html">Services</a></li>

                                <li class="menu-item"><a href="support.html">Support</a></li>
                            </ul>
                        </div>
                    </aside>
                    <aside class="column-1_4 widget widget_nav_menu">
                        <h5 class="widget_title">Navigate</h5>
                        <div class="menu-footer-navigate-container">
                            <ul id="menu-footer-navigate" class="menu">
                                <li class="menu-item current-menu-item"><a href="index-2.html">Home</a></li>
                                <li class="menu-item"><a href="post-formats.html">Blog</a></li>
                                <li class="menu-item"><a href="contacts.html">Contacts</a></li>
                            </ul>
                        </div>
                    </aside>
                    <aside class="column-1_4 widget widget_text">
                        <h5 class="widget_title">Subscribe</h5>
                        <div class="textwidget">
                            <ul class="trx_addons_list  icons">
                                <li class="icon-facebook"><a href="#">Facebook</a></li>
                                <li class="icon-twitter"><a href="#">Twitter</a></li>
                                <li class="icon-linkedin"><a href="#">Linkedin</a></li>
                                <li class="icon-gplus"><a href="#">Gplus</a></li>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright_wrap scheme_dark">
        <div class="copyright_wrap_inner">
            <div class="content_wrap">
                <div class="copyright_text">
                    <div class="columns_wrap">
                        <div class="column-1_2"><a href="http://zorandev.com/"> Develop by Zoran Shefot Bogoevski ©. All rights reserved.</a></div>
                        <div class="column-1_2">
                            <a href="#">Terms of use</a> and
                            <a href="#">Privacy Policy</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
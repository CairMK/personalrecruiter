@extends('layouts.main')
@section('content')
    <div class="top_panel_title_wrap">
        <div class="content_wrap">
            <div class="top_panel_title">
                <div class="page_title">
                    <h3 class="page_caption">All Posts</h3>
                </div>
                <div class="breadcrumbs">
                    <a class="breadcrumbs_item home" href="/">Home</a>
                    <span class="breadcrumbs_delimiter"></span>
                    <span class="breadcrumbs_item current">All Posts</span>
                </div>
            </div>
        </div>
    </div>
    <div class="chess_wrap posts_container">
        @foreach($blog as $blogs)
            <article class="post_item post_layout_chess post_layout_chess_1 post has-post-thumbnail hentry"
                     id="post_210">
                <a id="sc_anchor_post_210" class="sc_anchor"
                   title="Skills, Capabilities &amp; Characteristic Assessment" data-icon="" data-url=""></a>
                <div class="post_featured with_thumb hover_dots trx-stretch-height" style="height: 531px;">
                    <img src="/assets/images/1.jpg"
                         class="attachment-hr_advisor-thumb-original size-hr_advisor-thumb-original"
                         alt="">
                    <div class="mask"></div>
                    <a href="{{ route('blog_post', $blogs->slug) }}" aria-hidden="true" class="icons">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                </div>
                <div class="post_inner">
                    <div class="post_inner_content">
                        <div class="post_header entry-header">
                            <h3 class="post_title entry-title">
                                <a href="{{ route('blog_post', $blogs->slug) }}" rel="bookmark">{{ $blogs->title }}</a>
                            </h3>
                        </div>
                        <div class="post_content entry-content">
                            <div class="post_content_inner">
                                <p>
                                    {{$blogs->description}}
                                </p>
                            </div>
                            <p>
                                <a class="more-link sc_button_hover_slide_left"
                                   href="{{ route('blog_post', $blogs->slug) }}">Read
                                    more</a>
                            </p>
                        </div>
                    </div>
                </div>
            </article>
        @endforeach
        {{ $blog->links() }}
    </div>
@endsection